#include "ExpressionFactory.h"
#include "CogDefuzz.h"
#include "FuzzyFactory.h"
#include "IsRamp.h"
#include "IsGaussian.h"
#include "IsTrapeze.h"
#include "IsTriangle.h"
#include "AndMin.h"
#include "OrPlus.h"
#include "OrMax.h"
#include "ThenMin.h"
#include "AggPlus.h"
#include "NotMinus1.h"
#include "AggMax.h"
#include "SugenoDefuzz.h"
#include "ValueModel.h"

using namespace Core;
using namespace Fuzzy;
using namespace std;

int main() {
	//ExpressionFactory<float> expFact = ExpressionFactory<float>();
	//CogDefuzz<float>* cd = new CogDefuzz<float>(0, 15, 0.01);
	//FuzzyFactory<float> fuzFact = FuzzyFactory<float>(new BinaryShadowExpression<float>(new AndMin<float>()), new BinaryShadowExpression<float>(new OrMax<float>()), new BinaryShadowExpression<float>(new ThenMin<float>()), new BinaryShadowExpression<float>(new AggPlus<float>()), new BinaryShadowExpression<float>(cd), new UnaryShadowExpression<float>(new NotMinus1<float>()));
	//
	//cout << cd->evaluate((Expression<float>*) new IsTriangle<float>(5, 10, 15), (Expression<float>*) new ValueModel<float>())<<endl;
	////cout << cd->evaluate((Expression<float>*) new IsGaussian<float>(5, 2), (Expression<float>*) new ValueModel<float>())<<endl;
	////cout << cd->evaluate((Expression<float>*) new IsRamp<float>(3, 6, IsRamp<float>::dir::Down), (Expression<float>*) new ValueModel<float>())<<endl;
	//////e.PrintShape(cout, typename Evaluator<float>::Shape(Evaluator<float>::BuildShape(0, 15, 0.1, (Expression<float>*) new IsTrapeze<float>(0, 5, 10, 15, concavite::CONVEXE), (Expression<float>*) new ValueModel<float>())));
	////
	////cout << fuzFact.newDefuzz((Expression<float>*) new IsTriangle<float>(5, 10, 15), (Expression<float>*) new ValueModel<float>())->evaluate() << endl;
	////cout << fuzFact.newDefuzz((Expression<float>*) new IsGaussian<float>(5, 2), (Expression<float>*) new ValueModel<float>())->evaluate() << endl;
	////cout << fuzFact.newDefuzz((Expression<float>*) new IsRamp<float>(3, 6, IsRamp<float>::dir::Down), (Expression<float>*) new ValueModel<float>())->evaluate() << endl;
	////cout << fuzFact.newDefuzz((Expression<float>*) new IsTrapeze<float>(0, 5, 10, 15, concavite::CONVEXE), (Expression<float>*) new ValueModel<float>())->evaluate() << endl;
	//////e.PrintShape(cout, typename Evaluator<float>::Shape(Evaluator<float>::BuildShape(0, 10, 0.1, (Expression<float>*) new IsRamp<float>(3, 6, IsRamp<float>::Down), (Expression<float>*) new ValueModel<float>())));

	//ValueModel<float>* vm = new ValueModel<float>();
	//vm->setValue((fuzFact.newOr((Expression<float>*) fuzFact.newIs(new IsTriangle<float>(5, 10, 15), new ValueModel<float>(5)), (Expression<float>*) fuzFact.newIs(new IsGaussian<float>(5, 2), new ValueModel<float>(5))))->evaluate());
	////cout << fuzFact.newDefuzz((Expression<float>*) fuzFact.newThen((Expression<float>*) vm, (Expression<float>*) new IsTriangle<float>(5,10,15)), (Expression<float>*) new ValueModel<float>())->evaluate()<<endl;
	//e.PrintShape(cout, typename Evaluator<float>::Shape(Evaluator<float>::BuildShape(0, 15, 0.1, (Expression<float>*) fuzFact.newThen((Expression<float>*) vm, (Expression<float>*) new IsTriangle<float>(5, 10, 15)), (Expression<float>*) new ValueModel<float>())));

	//operators 
	NotMinus1<float> opNot;
	AndMin<float> opAnd;
	OrMax<float> opOr;
	ThenMin<float> opThen;
	CogDefuzz<float> opDefuzz(0, 25, 0.1);
	AggMax<float> opAgg;
	SugenoDefuzz<float> opSugenoDefuzz;
	
	//opConclusion.setCoeff(list);
	SugenoThen<float> opSugenoThen(0);

	//fuzzy expession factory
	FuzzyFactory<float> f((And<float>*) &opAnd, (Or<float>*)&opOr, (Then<float>*)&opThen, (Agg<float>*)&opAgg, &opDefuzz, (Not<float>*)&opNot, &opSugenoDefuzz);

	//membership function
	IsGaussian<float> poor(0,1.3);
	IsGaussian<float> good(5, 1.3);
	IsGaussian<float> excellent(10, 1.3);
	IsRamp<float> rancid(1, 4, IsRamp<float>::dir::Down);
	IsRamp<float> delicious(6, 9, IsRamp<float>::dir::Up);
	IsTriangle<float> cheap(0,5,10);
	IsTriangle<float> average(7.5,12.5,17.5);
	IsTriangle<float> generous(15,20,25);

	//values
	ValueModel<float> service(7);
	ValueModel<float> food(10);
	ValueModel<float> tips(3);

	Expression<float>* r = f.newAgg(
		f.newAgg(
			f.newThen(
				f.newOr(
					f.newIs(&poor, &service),
					f.newIs(&rancid, &food)
				),
				f.newIs(&cheap, &tips)),
			f.newThen(
				f.newIs(&good, &service),
				f.newIs(&average, &tips))),
		f.newThen(
			f.newOr(
				f.newIs(&excellent, &service),
				f.newIs(&delicious, &food)
			),
			f.newIs(&generous, &tips)));
	
	//defuzzification
	Expression<float> *system = f.newDefuzz(r, &tips);
	cout << "[";
	for (unsigned int j = 0; j < 11; j++) {


		food.setValue(j);
		Expression<float>* wi[3];
		Expression<float>* zi[3];

		vector<Expression<float>*> vars(2);
		vars[0] = &service;
		vars[1] = &food;
		//vars[0] = f.newIs(&poor, &service);
		//vars[1] = f.newIs(&rancid, &food);
		vector<float> list(3);
		list[0] = 1;
		list[1] = 1;
		list[2] = 0;

		SugenoConclusion<float> opConclusion(list);

		zi[0] = f.newSugenoConclusion(&opConclusion, vars);
		wi[0] = f.newOr(vars[0], vars[1]);

		//vars[0] = f.newIs(&good, &service);
		//vars[1] = f.newIs(new IsTriangle<float>(0, 0, 0), &food);
		vector<float> list2(3);
		list2[0] = 5;
		list2[1] = 5;
		list2[2] = 0;

		SugenoConclusion<float> opConclusion2(list2);
		zi[1] = f.newSugenoConclusion(&opConclusion2, vars);
		wi[1] = f.newOr(vars[0], vars[1]);

		//vars[0] = f.newIs(&excellent, &service);
		//vars[1] = f.newIs(&delicious, &food);
		vector<float> list3(3);
		list3[0] = 1;
		list3[1] = 1;
		list3[2] = 0;

		SugenoConclusion<float> opConclusion3(list3);
		zi[2] = f.newSugenoConclusion(&opConclusion3, vars);
		wi[2] = f.newOr(vars[0], vars[1]);


		vector<Expression<float>*> then;
		for (unsigned int i = 0; i < 3; i++) {
			then.push_back(f.newSugenoThen(&opSugenoThen, wi[i], zi[i]));
		}

		
		cout << f.newSugenoDefuzz(then)->evaluate() << ", ";
	}
	cout << "]" << endl;

	//apply input
	//float s;
	//while(true)  {
	//	cout << "service : ";
	//	cin >> s;
	//	service.setValue(s);
	//	cout << r->evaluate();
	//	cout << "tips -> " << system->evaluate() << endl;
	//}
}