#include "BinaryExpressionModel.h"
#include "ValueModel.h"
#include "AndMin.h"
#include "AndMult.h"
#include "OrMax.h"
#include "OrPlus.h"
#include "ThenMin.h"
#include "ThenMult.h"
#include "AggMax.h"
#include "AggPlus.h"
#include "IsTriangle.h"
#include <iostream>


using namespace Core;
using namespace Fuzzy;
using namespace std;

//int main() {
//	BinaryExpressionModel<float> u0 = BinaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(25),(Expression<float>*) new ValueModel<float>(14), (BinaryExpression<float>*) new AndMin<float>());
//	cout << u0.evaluate() << endl;
//	BinaryExpressionModel<float> u1 = BinaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(25),(Expression<float>*) new ValueModel<float>(14), (BinaryExpression<float>*) new AndMult<float>());
//	cout << u1.evaluate() << endl;
//	BinaryExpressionModel<float> u2 = BinaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(25),(Expression<float>*) new ValueModel<float>(14), (BinaryExpression<float>*) new OrMax<float>());
//	cout << u2.evaluate() << endl;
//	BinaryExpressionModel<float> u3 = BinaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(25),(Expression<float>*) new ValueModel<float>(14), (BinaryExpression<float>*) new OrPlus<float>());
//	cout << u3.evaluate() << endl;
//	BinaryExpressionModel<float> u4 = BinaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(25),(Expression<float>*) new ValueModel<float>(14), (BinaryExpression<float>*) new ThenMin<float>());
//	cout << u4.evaluate() << endl;
//	BinaryExpressionModel<float> u5 = BinaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(25),(Expression<float>*) new ValueModel<float>(14), (BinaryExpression<float>*) new ThenMult<float>());
//	cout << u5.evaluate() << endl;
//	BinaryExpressionModel<float> u6 = BinaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(25),(Expression<float>*) new ValueModel<float>(14), (BinaryExpression<float>*) new AggMax<float>());
//	cout << u6.evaluate() << endl;
//	BinaryExpressionModel<float> u7 = BinaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(25),(Expression<float>*) new ValueModel<float>(14), (BinaryExpression<float>*) new AggPlus<float>());
//	cout << u7.evaluate() << endl;
//}