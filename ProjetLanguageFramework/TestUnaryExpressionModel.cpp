#include "UnaryExpressionModel.h"
#include "ValueModel.h"
#include "NotMinus1.h"
#include "IsTriangle.h"
#include <iostream>


using namespace Core;
using namespace Fuzzy;
using namespace std;

//int main() {
//	UnaryExpressionModel<float> u0 = UnaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(0.8), (UnaryExpression<float>*) new NotMinus1<float>());
//	cout << u0.evaluate() << endl;
//	UnaryExpressionModel<float> u1 = UnaryExpressionModel<float>((Expression<float>*) new ValueModel<float>(3.5), (UnaryExpression<float>*) new IsTriangle<float>(2,5,7));
//	cout << u1.evaluate() << endl;
//}